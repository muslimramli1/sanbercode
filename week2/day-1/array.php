<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
    <h1>Berlatih Array</h1>
        
    <?php 
        echo "<h3> Soal 1 </h3>";
        /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
        $kids = ["Mike","Dustin", "Will", "Lucas", "Max", "Eleven"];// Lengkapi di sini
        $adults= ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
        
        
        echo "<h3> Soal 2</h3>";
        /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
        echo("Total kids adalah: ");
        echo count($kids);
        echo"<br>";
        print_r ($kids);
        echo"<br><br>";
        echo("Total Adults adalah: ");
        echo count($adults);
        echo"<br>";
        print_r($adults);
        // echo "Cast Stranger Things: ";
        // echo "<br>";
        // echo "Total Kids: " ;
        //  // Berapa panjang array kids
        // echo "<br>";
        // echo "<ol>"; 
        // echo "<li> $kids[0] </li>";
        // // Lanjutkan

        // echo "</ol>";
        
        // echo "Total Adults: " ;// Berapa panjang array adults
        // echo "<br>";
        // echo "<ol>";
        // echo "<li> $adults[0] </li>";
        // // Lanjutkan

        // echo "</ol>";

        /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array (Array Multidimensi)

            
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"
            
        */

        $arrayku = [["will Byers", 12, "will the wise", "Alive"],
                    ["Mike Wheeler", 12, "Dungeon Master", "Alive"],[
                    "Jim Hopper", 43, "Chief Hopper", "Deceased"],
                    ["Eleven", 12, "El", "Alive"]]
        
    ?>
</body>
</html>