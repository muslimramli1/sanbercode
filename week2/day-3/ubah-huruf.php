<?php
function ubah_huruf($string){
    $array=str_split($string);
    
    foreach ($array as $ganti) : 
        
        $next = chr(ord($ganti)+1);
        echo $next;

    endforeach;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>