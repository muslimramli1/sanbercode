<?php
function tukar_besar_kecil($string){
    $array=str_split($string);
    foreach ($array as $ganti) : 
        
        if ($ganti== strtoupper($ganti)) {
            echo strtolower($ganti);
        }
        else{
            echo strtoupper($ganti);
        }

    endforeach;
  

}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "<br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "<br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "<br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "<br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>